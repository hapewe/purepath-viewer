use serde::{Deserialize, Serialize};
use wasm_bindgen::UnwrapThrowExt;
use web_sys::HtmlElement;
use yew::prelude::*;
use gloo::events::EventListener;

#[derive(Serialize, Deserialize)]
struct GreetArgs<'a> {
    name: &'a str,
}

#[function_component(App)]
pub fn app() -> Html {
    let main_node_ref = use_node_ref();
    
    use_effect_with_deps(
        {
            move |_| {
                let mut file_open_listener = None;

                if let Some(element) = web_sys::window() {
                    // Create your Callback as you normally would
                    let onfileopen = Callback::from(move |_: Event| {
                        web_sys::window().expect_throw("Window is not defined").alert_with_message("File Open").unwrap_throw();
                    });

                    // Create a Closure from a Box<dyn Fn> - this has to be 'static
                    let listener = EventListener::new(
                        &element,
                        "onFileLoad",
                        move |e| onfileopen.emit(e.clone())
                    );

                    file_open_listener = Some(listener);
                }

                move || drop(file_open_listener)                
            }
        },main_node_ref.clone()
    );

    html! {
        <main class="container" ref={main_node_ref}>
            <div class="row">
                <a href="https://tauri.app" target="_blank">
                    <img src="public/tauri.svg" class="logo tauri" alt="Tauri logo"/>
                </a>
                <a href="https://yew.rs" target="_blank">
                    <img src="public/yew.png" class="logo yew" alt="Yew logo"/>
                </a>
            </div>

            <p>{"Click on the Tauri and Yew logos to learn more. And check"}</p>

            <p>
                {"Recommended IDE setup: "}
                <a href="https://code.visualstudio.com/" target="_blank">{"VS Code"}</a>
                {" + "}
                <a href="https://github.com/tauri-apps/tauri-vscode" target="_blank">{"Tauri"}</a>
                {" + "}
                <a href="https://github.com/rust-lang/rust-analyzer" target="_blank">{"rust-analyzer"}</a>
            </p>

            <form class="row">
                <input id="greet-input" placeholder="Enter a name..." />
                <button type="submit">{"Greet"}</button>
            </form>

            <p><b>{ "xxx" }</b></p>
        </main>
    }
}
