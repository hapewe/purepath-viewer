#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use log::{info, warn};
use tauri::{CustomMenuItem, Menu, MenuItem, Submenu};   
use simplelog::{Config, SimpleLogger};

// Prevents additional console window on Windows in release, DO NOT REMOVE!!

fn main() {
    //console_log::init_with_level(log::Level::Trace).unwrap();
    SimpleLogger::init(log::LevelFilter::Info, Config::default()).unwrap();

    let mut menu = Menu::os_default("Purepath Viewer");
    menu = menu.add_submenu(Submenu::new("File", Menu::new()
        .add_item(CustomMenuItem::new("open_file", "Open"))
        .add_native_item(MenuItem::Separator)
        .add_native_item(MenuItem::Quit)));

    menu.items.swap_remove(1);
    tauri::Builder::default()
        .menu(menu.clone())
        .on_menu_event(|event| {
            match event.menu_item_id() {
                "quit" => {
                    info!("Quit menu item clicked, quitting application");
                    std::process::exit(0);
                }
                "open_file" => {
                    std::thread::sleep(std::time::Duration::from_millis(1000));
                    match event.window().emit("onFileLoad", "onFileLoad") {
                        Ok(_) => {
                            info!("on_file_load event emitted");
                        }
                        Err(e) => {
                            warn!("error: {:?}", e);
                        }
                    }
                }
                _ => {}
            }
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
